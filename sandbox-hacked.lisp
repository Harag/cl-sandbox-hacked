;;TODO: Between read-no-eval, read-eval and eval-valid-forms there is a lot of duplicate code
;;      but adding more generic functions will just layer calls upon calls. Macro?

(in-package :cl-sandbox-hacked)

(defvar *allowed-sandbox-symbols* nil)
(defvar *allowed-sandbox-functions* nil)

(defun sandbox-allowed-symbols ()
  (loop :for symbol :being :the :symbol :in (find-package 'sandbox-cl)
     :when (not (get symbol :sandbox-locked))
     :do
       (if (fboundp symbol)
	   (push symbol *allowed-sandbox-functions*)
	   (push symbol *allowed-sandbox-symbols*))))

(sandbox-allowed-symbols)

(defvar *allowed-packages-symbols* nil)
(defvar *allowed-packages-functions* nil)

(defun set-allowed-symbol (symbol)
  (if (fboundp symbol)
      (push symbol *allowed-packages-functions*)
      (push symbol *allowed-packages-symbols*)))

(defun reset-sandbox ()  
  (setf *allowed-packages-symbols* nil)
  (setf *allowed-packages-functions* nil)
  (ignore-errors
    (delete-package kunru-sandbox:*sandbox*))
  (make-package kunru-sandbox:*sandbox* :use '(#:sandbox-cl)))

(defun get-package-symbols (packages &optional excluded-symbols)
  (let (symbols)
    (dolist (package packages)
      (do-external-symbols (s (find-package package))
	(unless (find s excluded-symbols :test 'equalp)
	  (push s symbols))))
    symbols))

(defun allow-symbols (symbols)
  (dolist (symbol symbols)
      (set-allowed-symbol symbol)))

(defun allow-package-symbols (packages &optional excluded-symbols)
  (unless *allowed-packages-symbols*
    (dolist (package packages)
	(do-external-symbols (symbol (find-package package))
	  (unless (find symbol excluded-symbols :test 'equalp)
	    (set-allowed-symbol symbol))))))

(defun translate-validate-form (form)
  (when (and (consp form)
             (sandbox-impl::circular-tree-p form))
    (error 'circular-list))
  (let ((cons-count 0))
    (labels ((translate (form)
               (typecase form
                 (cons (if (> (incf cons-count) sandbox-impl::*max-elements*)
                           (error 'dimension-error)
                           (cons (translate (car form))
                                 (translate (cdr form)))))
                 (number form)
                 (character form)
                 (pathname form)
                 (array (if (> (array-total-size form) sandbox-impl::*max-elements*)
                            (error 'dimension-error)
                            (let ((arr (make-array (array-dimensions form)
                                                   :element-type
                                                   (array-element-type form))))
                              (dotimes (i (array-total-size arr) arr)
                                (setf (row-major-aref arr i)
                                      (translate-validate-form
                                       (row-major-aref form i)))))))
                 (keyword form)
                 (symbol (if (fboundp form)
			     (or (find form *allowed-sandbox-functions*)
				 (find form *allowed-packages-functions*)
				 (error 'undefined-function :name form))
			     (if (or (find form *allowed-sandbox-symbols*)
				     (find form *allowed-packages-symbols*))
				 form
				 (intern (symbol-name form) kunru-sandbox:*sandbox*))))
                 (t (error 'unsupported-type :type (type-of form))))))
      (translate form))))

(defun read-no-eval (forms &key packages exclude-symbols)
  "Returns forms and/or any messages."
  (unless (or (find-package kunru-sandbox:*sandbox*) (kunru-sandbox:reset))    
    (return-from read-no-eval "SANDBOX-PACKAGE-ERROR: Sandbox package not found."))

  (allow-package-symbols packages exclude-symbols)

  (let ((validated-forms)
	(msg))

    (labels ((sexp-read (sexps)
	       (let (values)
		 (if (listp (car sexps))		     
		     (dolist (sexp sexps)		 
		       (push (translate-validate-form sexp) values))
		     (push (translate-validate-form sexps) values))
		 (reverse values)))
	     (sread (string)
	       (let (values)
		 (with-input-from-string (s string)		 
		   (loop for sexp = (read s nil)
		      while sexp
		      do
			(if (listp (car sexp))
			    (dolist (sexpx sexp)
			      (push (translate-validate-form sexpx)
				      values))
			    (push (translate-validate-form sexp)
				  values))))
		 (reverse values)))            
	     (muffle (c)
	       (declare (ignore c))
	       (when (find-restart 'muffle-warning)
		 (muffle-warning))))
      
      (setf validated-forms
		  (if (stringp forms)
		      (sread forms)
		      (sexp-read forms))))
    (values validated-forms msg)))

(defun read-eval (forms &key packages exclude-symbols)
  "Returns eval values and/or any messages."

  (unless (or (find-package kunru-sandbox:*sandbox*) (kunru-sandbox:reset))
    (return-from read-eval (values nil "SANDBOX-PACKAGE-ERROR: Sandbox package not found.")))

  (allow-package-symbols packages exclude-symbols)

  (kunru-sandbox::with-sandbox-env
   (let ((values)
	 (msg))
       
     (flet ((sexp-read (sexps)
	      (let (values)
		(if (listp (car sexps))
		    (dolist (sexp sexps)
		      (push (multiple-value-list
			     (eval
			      (translate-validate-form sexp)))
			    values))
		    (push (multiple-value-list
			   (eval
			    (translate-validate-form sexps)))
			  values))   
		(reverse values)))
	    (sread (string)
	      (let (values) 
		(with-input-from-string (s string)		  
		  (loop for sexp = (read s nil)
		     while sexp
		     do
		       (multiple-value-list
			(if (listp (car sexp))
			    (dolist (sexpx sexp)
			      (push (multiple-value-list
				     (eval
				      (translate-validate-form sexpx)))
				    values))
			    (push (multiple-value-list
				   (eval
				    (translate-validate-form sexp)))
				  values)))))
		(reverse values)))            
	    (muffle (c)
	      (declare (ignore c))
	      (when (find-restart 'muffle-warning)
		(muffle-warning))))
       (setf values (if (stringp forms)
			      (sread forms)
			      (sexp-read forms))))
     (values values msg))))


(defun eval-valid-forms (forms)
  "Returns eval values and/or any messages. NO checking is done so it is up to the user to be sure the code is safe."
  
  (kunru-sandbox::with-sandbox-env
     (let ((values)
	   (msg))
       
       (flet ((sexp-read (sexps)
		(let (values)
		  (if (listp (car sexps))
		      (dolist (sexp sexps)
			(push (multiple-value-list
			       (eval
				sexp))
			      values))
		      (push (multiple-value-list
			       (eval
				sexps))
			      values))   
		  (reverse values)))
	      (sread (string)
		(let (values)
		  (with-input-from-string (s string)		    
		    (loop for sexp = (read s nil)
		       while sexp
		       collect
			 (multiple-value-list
			  (if (listp (car sexp))
			      (dolist (sexpx sexp)
				(push (multiple-value-list
				       (eval
					sexpx))
				      values))
			      (push (multiple-value-list
				     (eval
				      sexp))
				    values))) ))
		  (reverse values)))            
	      (muffle (c)
		(declare (ignore c))		
		(when (find-restart 'muffle-warning)
		  (muffle-warning))))
	 (setf values (if (stringp forms)
				(sread forms)
				(sexp-read forms))))
       (values values msg))))
