(in-package :common-lisp-user)

(defpackage :cl-sandbox-hacked
  (:use :cl)
  
  (:export

   :*allowed-sandbox-symbols*
   :*allowed-sandbox-functions*
   :*allowed-packages-symbols*
   :*allowed-packages-functions*  
   :validate-code
   :set-allowed-symbol
   :reset-sandbox
   :get-package-symbols
   :allow-symbols
   :allow-package-symbols
   :read-no-eval
   :read-eval
   :eval-valid-forms))



