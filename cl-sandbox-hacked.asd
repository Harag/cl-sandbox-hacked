(defsystem "cl-sandbox-hacked"
  :description "Extends cl-sandbox to do stuff I want."
  :version "2019.11.22"
  :author "Phil Marneweck <phil@psychedelic.co.za>"
  :licence "MIT"
  :depends-on ("kunru-sandbox")
  :components ((:file "packages")
	       (:file "sandbox-hacked" :depends-on ("packages"))))
